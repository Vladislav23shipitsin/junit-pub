package com.lineate.traineeship;

import org.junit.Before;
import org.junit.Test;


import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class TestService {
    private static UserService USER_SERVICE;
    private static EntityService ENTITY_SERVICE;

    @Before
    public  void initService(){
        USER_SERVICE = new ServiceFactory().createUserService();
        ENTITY_SERVICE = new ServiceFactory().createEntityService();
    }

    @Test
    public void testAllGroups(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        Group group2 = USER_SERVICE.createGroup("group2", Arrays.asList(Permission.read));
        Group group3 = USER_SERVICE.createGroup("group3", Arrays.asList(Permission.write));
        User user1 = USER_SERVICE.createUser("user1",group1);
        group2.addUser(user1);
        group3.addUser(user1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");
        Entity entity1 = ENTITY_SERVICE.getEntity("entity1");
        Collection<Group> groupsResult = entity1.getGroups();

        assertAll("entity1 group's",
                () -> assertEquals(3,groupsResult.size()),
                () -> assertTrue(groupsResult.contains(group1)),
                () -> assertTrue(groupsResult.contains(group2)),
                () -> assertTrue(groupsResult.contains(group3))
        );
    }

    @Test
    public void testChangeListGroups(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        Group group2 = USER_SERVICE.createGroup("group2", Arrays.asList(Permission.write));
        User user1 = USER_SERVICE.createUser("user1",group1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");
        Collection<Group> groupsResult1 = ENTITY_SERVICE.getEntity("entity1").getGroups();

        groupsResult1.add(group2);

        Collection<Group> actualResult = ENTITY_SERVICE.getEntity("entity1").getGroups();

        assertAll("entity1 group's",
                () -> assertEquals(1,actualResult.size()),
                () -> assertTrue(actualResult.contains(group1))
        );
    }


}

package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestUser {
    private static UserService USER_SERVICE;
    @BeforeAll
    public static void initService(){
        USER_SERVICE = new ServiceFactory().createUserService();
    }

    @Test
    public void testGet(){
        Group group = USER_SERVICE.createGroup("backend", Arrays.asList(Permission.read));
        User user = USER_SERVICE.createUser("Ivan",group);

        assertAll("user",
                () -> assertEquals("Ivan",user.getName()),
                () -> assertFalse(user.getGroups().isEmpty()),
                () -> assertTrue(user.getGroups().contains(group))
        );
    }
    @ParameterizedTest
    @ValueSource(strings = {"","  "})
    public void testWrongName(String wrongName){
        Group group = USER_SERVICE.createGroup("backend",Arrays.asList(Permission.read));
        User user = USER_SERVICE.createUser(wrongName,group);
        assertNull(user);
    }

}

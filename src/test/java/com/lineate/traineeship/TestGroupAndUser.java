package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class TestGroupAndUser {

    private static UserService USER_SERVICE;

    @BeforeAll
    public static void initService(){
        USER_SERVICE = new ServiceFactory().createUserService();
    }

    @Test
    public void testCreateUsersWithGroup(){
        Group group1 = USER_SERVICE.createGroup("backend", Arrays.asList(Permission.read));
        User user1 = USER_SERVICE.createUser("Ivan",group1);
        User user2 = USER_SERVICE.createUser("Petr",group1);

        Collection<User> resultUser = group1.getUsers();

        assertAll("resultUser",
                () -> assertNotNull(resultUser),
                () -> assertEquals(2,resultUser.size()),
                () -> assertTrue(resultUser.contains(user1)),
                () -> assertTrue(resultUser.contains(user2))
        );
    }
    @Test
    public void testAddUserInGroup(){
        Group group1 = USER_SERVICE.createGroup("backend", Arrays.asList(Permission.read));
        Group group2 = USER_SERVICE.createGroup("fullstack", Arrays.asList(Permission.write));
        User user = USER_SERVICE.createUser("Ivan",group1);

        group2.addUser(user);

        Collection<Group> resultGroups = user.getGroups();

        assertAll("group's user",
                () -> assertNotNull(resultGroups),
                () -> assertEquals(2,resultGroups.size()),
                () -> assertTrue(resultGroups.contains(group1)),
                () -> assertTrue(resultGroups.contains(group2))
        );

    }
}

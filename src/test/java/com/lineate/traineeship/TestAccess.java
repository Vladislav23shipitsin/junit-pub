package com.lineate.traineeship;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestAccess {
    private static UserService USER_SERVICE;
    private static EntityService ENTITY_SERVICE;

    @Before
    public  void initService(){
        USER_SERVICE = new ServiceFactory().createUserService();
        ENTITY_SERVICE = new ServiceFactory().createEntityService();
    }

    @Test
    public void testFullAccess(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        User user1 = USER_SERVICE.createUser("user1",group1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");

        assertEquals("value1",ENTITY_SERVICE.getEntityValue(user1,"entity1"));
        assertTrue(ENTITY_SERVICE.updateEntity(user1,"entity1","new value"));
    }

    @Test
    public void testGroupAccessRead(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        User user1 = USER_SERVICE.createUser("user1",group1);
        User user2 = USER_SERVICE.createUser("user2",group1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");

        assertNotNull(ENTITY_SERVICE.getEntityValue(user2,"entity1"));
    }
    @Test
    public void testGroupNotAccessWrite(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        User user1 = USER_SERVICE.createUser("user1",group1);
        User user2 = USER_SERVICE.createUser("user2",group1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");

        assertFalse(ENTITY_SERVICE.updateEntity(user2,"entity1","new value"));
    }
    @Test
    public void testGroupAccessWrite(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.write));
        User user1 = USER_SERVICE.createUser("user1",group1);
        User user2 = USER_SERVICE.createUser("user2",group1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");
        assertTrue(ENTITY_SERVICE.updateEntity(user2,"entity1","new value"));
    }
    @Test
    public void testGroupNotAccessRead(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.write));
        User user1 = USER_SERVICE.createUser("user1",group1);
        User user2 = USER_SERVICE.createUser("user2",group1);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");

        assertNull(ENTITY_SERVICE.getEntityValue(user2,"entity1"));
    }
    @Test
    public void testNotAccessRead(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        Group group2 = USER_SERVICE.createGroup("group2", Arrays.asList(Permission.read,Permission.write));
        User user1 = USER_SERVICE.createUser("user1",group1);
        User user2 = USER_SERVICE.createUser("user2",group2);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");

        assertNull(ENTITY_SERVICE.getEntityValue(user2,"entity1"));
    }
    @Test
    public void testNotAccessWrite(){
        Group group1 = USER_SERVICE.createGroup("group1", Arrays.asList(Permission.read));
        Group group2 = USER_SERVICE.createGroup("group2", Arrays.asList(Permission.read,Permission.write));
        User user1 = USER_SERVICE.createUser("user1",group1);
        User user2 = USER_SERVICE.createUser("user2",group2);

        ENTITY_SERVICE.createEntity(user1,"entity1","value1");

        assertFalse(ENTITY_SERVICE.updateEntity(user2,"entity1","new value"));
    }





}

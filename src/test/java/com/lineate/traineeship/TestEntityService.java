package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestEntityService {

    private static EntityService ENTITY_SERVICE;
    private static UserService USER_SERVICE;
    private static Group GROUP;
    private static User USER;

    @BeforeAll
    public static void initServiceAndUser(){
        ENTITY_SERVICE = new ServiceFactory().createEntityService();
        USER_SERVICE  = new ServiceFactory().createUserService();
        GROUP = USER_SERVICE.createGroup("name group",Arrays.asList(Permission.read));
        USER =  USER_SERVICE.createUser("name user",GROUP);
    }

    public boolean createEntity(User user,String name,String value){
        return ENTITY_SERVICE.createEntity(user,name,value);
    }

    @Test
    public void testCreate(){
        boolean isCreate = createEntity(USER,"nameEntity","value entity");
        assertTrue(isCreate);
    }

    @ParameterizedTest
    @ValueSource(strings = {""," ","Ivan Ivanov","123456789012345678901234567890123"})
    public void testNotCreate(String wrongName){
        boolean isCreate = createEntity(USER,wrongName,"value entity");
        assertFalse(isCreate);
    }

    @Test
    public void testGetValue(){
        createEntity(USER,"name","value");
        String resultValue = ENTITY_SERVICE.getEntityValue(USER,"name");
        assertEquals("value",resultValue);
    }

    @Test
    public void testUpdate(){
        createEntity(USER,"name","value");
        String newValue = "new value Entity";
        boolean isUpdate = ENTITY_SERVICE.updateEntity(USER,"name",newValue);

        assertTrue(isUpdate);

        String resultValue = ENTITY_SERVICE.getEntityValue(USER,"name");
        assertEquals(newValue,resultValue);
    }

}

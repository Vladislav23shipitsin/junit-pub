package com.lineate.traineeship;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNull;


public class TestGroup {
    private static UserService USER_SERVICE;

    @BeforeAll
    public static void initService(){
        USER_SERVICE = new ServiceFactory().createUserService();
    }

    @Test
    public void testGet(){
        Collection<Permission> permissions = Arrays.asList(Permission.read,Permission.write);
        String name = "backend";
        Group group = USER_SERVICE.createGroup(name,permissions);

        assertAll("group",
                () -> Assertions.assertEquals(name,group.getName()),
                () -> Assertions.assertAll("permissions",
                        () -> Assertions.assertEquals(2,group.getPermissions().size()),
                        () -> Assertions.assertTrue(group.getPermissions().contains(Permission.read)),
                        () -> Assertions.assertTrue(group.getPermissions().contains(Permission.write)) )
                );
    }

    @ParameterizedTest
    @ValueSource(strings = {"","  "})
    public void createWrongName(String name){
        Group group = USER_SERVICE.createGroup(name, Arrays.asList(Permission.read));
        assertNull(group);
    }

    @Test
    public void testWrongPermission(){
        List<Permission> permissions = new ArrayList<>();
        Group group = USER_SERVICE.createGroup("name group",permissions);
        assertNull(group);
    }
}
